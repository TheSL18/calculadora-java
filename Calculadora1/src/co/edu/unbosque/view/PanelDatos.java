package co.edu.unbosque.view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class PanelDatos extends JPanel {
	private JLabel enum1;
	private JLabel enum2;
	private JLabel eoperaciones;
	
	private JTextField cnum1;
	private JTextField cnum2;
	
	public PanelDatos() {
		setLayout(null);
		inicializarComponentes();
		setVisible(true);
		setOpaque(false);
	}
	public void inicializarComponentes() {
		
		Border border = new LineBorder(new Color(0,0,0), 0, true);
		TitledBorder tb = new TitledBorder(border, "Panel de Datos");
		Font fuente = new Font("Tahoma", Font.BOLD, 12);
		tb.setTitleFont(fuente);
		tb.setTitleColor(Color.WHITE);
		setBorder(tb);
		 
		cnum1 = new JTextField();
		cnum1.setBounds(150, 25, 450, 20);
		add(cnum1);
		
		cnum2 = new JTextField();
		cnum2.setBounds(150, 75, 450, 20);
		cnum2.setVisible(false);
		add(cnum2);

		
		 
		enum1 = new JLabel("Ingrese un valor");
		enum1.setFont(fuente);
		enum1.setForeground(Color.white);
		enum1.setBounds(20, 25, 100, 20);
		add(enum1);
		
		enum2 = new JLabel("Ingrese un valor");
		enum2.setFont(fuente);
		enum2.setForeground(Color.white);
		enum2.setBounds(20, 75, 100, 20);
		enum2.setVisible(false);
		add(enum2);
		
		 
		eoperaciones = new JLabel("Operaciones");
		eoperaciones.setBounds(25, 150, 630, 100);
		add(eoperaciones); 
	
	
		
	}

	public JTextField getCnum2() {
		return cnum2;
	}
	public void setCnum2(JTextField cnum2) {
		this.cnum2 = cnum2;
	}
	public JLabel getEnum2() {
		return enum2;
	}
	public void setEnum2(JLabel enum2) {
		this.enum2 = enum2;
	}
	public JLabel getEnum1() {
		return enum1;
	}
	public void setEnum1(JLabel enum1) {
		this.enum1 = enum1;
	}
	public JLabel getEoperaciones() {
		return eoperaciones;
	}
	public void setEoperaciones(JLabel eoperaciones) {
		this.eoperaciones = eoperaciones;
	}
	public JTextField getCnum1() {
		return cnum1;
	}
	public void setCnum1(JTextField cnum1) {
		this.cnum1 = cnum1;
	}
	
}
