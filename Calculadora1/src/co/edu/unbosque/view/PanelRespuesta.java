package co.edu.unbosque.view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class PanelRespuesta extends JPanel {
	private JLabel erta;
	private JLabel enombre1;
	private JLabel enombre2;
	private JLabel enombre3;
	private JLabel enombre4;
	
	
	public PanelRespuesta() {
		setOpaque(false);
		setLayout(null);
		inicializarComponentes();
		setVisible(true);
	}
	public void inicializarComponentes() {
		Border border = new LineBorder(new Color(0,0,0), 0, true);
		TitledBorder tb = new TitledBorder(border, "Panel de Respuesta");
		Font fuente = new Font("Tahoma", Font.BOLD, 12);
		tb.setTitleFont(fuente);
		tb.setTitleColor(Color.WHITE);
		setBorder(tb);
		
		erta = new JLabel();
		erta.setForeground(Color.white);
		erta.setBounds(15, 35, 500, 20);
		add(erta);
		
		enombre1 = new JLabel("Nicolas Camacho");
		enombre1.setForeground(Color.white);
		enombre1.setBounds(500, 12, 630, 100);
		add(enombre1); 
		
		enombre2 = new JLabel("Kevin Muñoz");
		enombre2.setForeground(Color.white);
		enombre2.setBounds(500, 24, 630, 100);
		add(enombre2); 
		
		enombre3 = new JLabel("Samuel Viloria");
		enombre3.setForeground(Color.white);
		enombre3.setBounds(500, 36, 630, 100);
		add(enombre3); 
		
		enombre4 = new JLabel("Hecho por");
		enombre4.setForeground(Color.white);
		enombre4.setBounds(500, 0, 630, 100);
		add(enombre4); 
	}
	
	public JLabel getEnombre1() {
		return enombre1;
	}
	public JLabel getEnombre2() {
		return enombre2;
	}
	public JLabel getEnombre3() {
		return enombre3;
	}
	public JLabel getErta() {
		return erta;
	}
	public void setErta(JLabel erta) {
		this.erta = erta;
	}
	
}
