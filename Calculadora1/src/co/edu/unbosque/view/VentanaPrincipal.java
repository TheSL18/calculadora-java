package co.edu.unbosque.view;

import java.awt.Color;
import javax.swing.JFrame;

public class VentanaPrincipal extends JFrame {
	private PanelDatos pdatos;
	private PanelRespuesta prespuesta;
	private PanelOperaciones poperaciones;
	private FondoCalculadora fondoC;
	private Mensaje mensaje;
	
	public VentanaPrincipal() {
		FondoCalculadora fondoC = new FondoCalculadora();
		setTitle("Calculadora");
		setSize(700,450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground(new Color(15,190,230));
		setContentPane(fondoC);
		getContentPane().setLayout(null);
		inicializarComponentes();
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	public void inicializarComponentes() {
		pdatos = new PanelDatos();
		pdatos.setBounds(25, 25, 630, 100);
		getContentPane().add(pdatos);
		
		prespuesta = new PanelRespuesta();
		prespuesta.setBounds(25, 300, 630, 100);
		getContentPane().add(prespuesta);
		
		poperaciones = new PanelOperaciones();
		poperaciones.setBounds(25, 150, 630, 150);
		getContentPane().add(poperaciones);
		
	}

	public PanelDatos getPdatos() {
		return pdatos;
	}

	public void setPdatos(PanelDatos pdatos) {
		this.pdatos = pdatos;
	}

	public PanelRespuesta getPrespuesta() {
		return prespuesta;
	}

	public void setPrespuesta(PanelRespuesta prespuesta) {
		this.prespuesta = prespuesta;
	}

	public PanelOperaciones getPoperaciones() {
		return poperaciones;
	}

	public void setPoperaciones(PanelOperaciones poperaciones) {
		this.poperaciones = poperaciones;
	}

	public Mensaje getMensaje() {
		return mensaje;
	}

	public void setMensaje(Mensaje mensaje) {
		this.mensaje = mensaje;
	}
	

	
}
