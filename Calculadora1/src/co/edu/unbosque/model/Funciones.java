package co.edu.unbosque.model;

public class Funciones {
	private double num;
	
	public Funciones() {
		num = 0;
	}

//Validadores
	public boolean validarDecimal(long decimal) {
//Decimal pasa la validaciónn con el hecho de que sea entero
		if(decimal < 0) {
			return false;
		}
		return true;
	}

	public boolean validarBinario(String binario) {
//Comprobar si solo se compone de unos y ceros
		String binarioComoCadena = String.valueOf(binario);
		for (int i = 0; i < binarioComoCadena.length(); ++i) {
			char caracter = binarioComoCadena.charAt(i);
			if (caracter != '0' && caracter != '1')
				return false;
		}
		return true;
	}

	public boolean validarHexadecimal(String hexadecimal) {
//Comprobar si solo tiene numeros del 0 al 9 y letras de la A a la F
		hexadecimal = hexadecimal.toUpperCase();
		String caracteresHexadecimales = "0123456789ABCDEF";
		for (int i = 0; i < hexadecimal.length(); ++i) {
			if (caracteresHexadecimales.indexOf(hexadecimal.charAt(i)) >=0) {
				return true;
			}
			if (caracteresHexadecimales.indexOf(hexadecimal.charAt(i)) ==-1) {
				return false;
			}
		}return false;
}	

	
	
	
	
	
	public String decimalABinario(long decimal) {
		String binario = "";
		while (decimal > 0) {
			binario = decimal % 2 + binario;
			decimal /= 2;
		}
		return binario;
	}

	public String decimalAHexadecimal(long decimal) {
		String hexadecimal = "";
		int aux = 0;
		char[] caracteresHexadecimales = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
		'F' };
		while (decimal > 0) {
			aux = (int) (decimal % 16);
			hexadecimal=caracteresHexadecimales[aux]+hexadecimal;
			decimal /= 16;
		}
		return hexadecimal;
	}

	public int binarioADecimal(String binario) {
		int decimal = 0;
		int potencia = 0;
		char[] numeros = binario.toCharArray();
		for(int i=binario.length()-1;i>=0;i--) {
			decimal = (int) (decimal + ((numeros[i] - 48)  * Math.pow(2, potencia)));
			potencia++;
		}
		return decimal;
	}

	public int hexadecimalADecimal(String hexadecimal) {
		String caracteresHexadecimales = "0123456789ABCDEF";
		hexadecimal = hexadecimal.toUpperCase();
		int decimal = 0;
		for (int i = 0; i < hexadecimal.length(); ++i) {
			decimal = 16 * decimal + caracteresHexadecimales.indexOf(hexadecimal.charAt(i));
		}
		return decimal;
	}
	
	public String hexadecimalABinario(String hexadecimal) {
		String caracteresHexadecimales = "0123456789ABCDEF";
		String[] stringHexadecimales = { "0000","0001","0010","0011","0100", "0101", "0110", "0111"
				,"1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};
		String binario = "";
		hexadecimal = hexadecimal.toUpperCase();
		int decimal = 0;
		for (int i = hexadecimal.length()-1; i >= 0; --i) {
			decimal =caracteresHexadecimales.indexOf(hexadecimal.charAt(i));
			binario = stringHexadecimales[decimal] + binario;
			}
		return binario;
	}
	
	public String binarioAHexadecimal(String binario) {
		String hexa = "";
		int decimal = 0;
		int potencia = 0;
		int a = 3;
		int b = 1;
		char[] numeros = binario.toCharArray();
		char[] hexnum = new char[4];
		
		while(b<numeros.length) {
			hexnum[0] = '0';
			hexnum[1] = '0';
			hexnum[2] = '0';
			hexnum[3] = '0';
		
			for(int y = numeros.length-b; a>=0;y--) {       
				if(y>=0) {
					hexnum[a] = numeros[y];	
				}
				a--;
			}
			for(int i=hexnum.length-1;i>=0;i--) {
				decimal = (int) (decimal + ((hexnum[i] - 48)  * Math.pow(2, potencia)));
				potencia++;
			}
			char[] caracteresHexadecimales = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
			'F' };	
			hexa = caracteresHexadecimales[decimal] + hexa;
			decimal = 0;
			potencia = 0;
			a = 3;
			b = b+4;
		}
		return hexa;
		
	}
	
	public String sumaBinarios(String binario1, String binario2){
		char[] bin1 = binario1.toCharArray();
		char[] bin2 = binario2.toCharArray();
		char[] arrayBin;
		int lengthBin1 = bin1.length-1;
		int lengthBin2 = bin2.length-1;
		int acumulado = 0;
		int lengthFinal = 0;
		String total = "";
		
		while(lengthBin1 >= 0 && lengthBin2 >= 0) {
			if(bin1[lengthBin1]=='1' && bin2[lengthBin2]=='1'){
				if(acumulado == 1) {
					total = 1 + total;
					acumulado = 1;
				}else {
					total = 0 + total;
					acumulado = 1;
				}
				
			}else if(bin1[lengthBin1]=='1' && bin2[lengthBin2]=='0'){
				if(acumulado == 1) {
					total = 0 + total;
					acumulado = 1;
				}else {
					total = 1 + total;
					acumulado = 0;
				}
				
			}else if(bin1[lengthBin1]=='0' && bin2[lengthBin2]=='1'){
				if(acumulado == 1) {
					total = 0 + total;
					acumulado = 1;
				}else {
					total = 1 + total;
					acumulado = 0;
				}
				
			}else if(bin1[lengthBin1]=='0' && bin2[lengthBin2]=='0'){
				if(acumulado == 1) {
					total = 1 + total;
					acumulado = 1;
				}else {
					total = 0 + total;
					acumulado = 0;
				}
				
			}
			lengthBin1 -= 1;
			lengthBin2 -= 1;
		}
		
		
		while(lengthBin1 >= 0) {
			if(bin1[lengthBin1]=='1'){
				if(acumulado == 1) {
					total = 0 + total;
					acumulado = 1;
				}else {
					total = 1 + total;
					acumulado = 0;
				}
				
			}else if(bin1[lengthBin1]=='0'){
				if(acumulado == 1) {
					total = 1 + total;
					acumulado = 0;
				}else {
					total = 0 + total;
					acumulado = 1;
				}
			}
			lengthBin1 -= 1;
		}
		
		while(lengthBin2 >= 0) {
			if(bin2[lengthBin2]=='1'){
				if(acumulado == 1) {
					total = 0 + total;
					acumulado = 1;
				}else {
					total = 1 + total;
					acumulado = 0;
				}
				acumulado = 0;
			}else if(bin2[lengthBin2]=='0'){
				if(acumulado == 1) {
					total = 1 + total;
					acumulado = 0;
				}else {
					total = 0 + total;
					acumulado = 1;
				}
				acumulado = 0;
			}
			lengthBin2 -= 1;
		}
		if(acumulado >0) {
			total = 1 + total;
			acumulado = 0;
		}

		return total;
	}

	public double getNum() {
		return num;
	}

	public void setNum(double num) {
		this.num = num;
	}
	
}